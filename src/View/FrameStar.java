package View;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Control.Controller;


public class FrameStar extends JFrame {
  private JFrame frame;
  private JPanel panel;
  private JLabel inputLabel;
  private JTextArea textResult;
  private JTextField textInput;
  private JComboBox comboBox;
  private JButton button;
  
  
  public FrameStar(){

	 
	  getContentPane().setLayout(new GridLayout(0, 1, 0, 3));
	  

	  createTextField();
      createButton();
      createComboBox();
      createPanel();


	  setVisible(true);
	  setSize(500, 320);
	  setResizable(false);
	  setLocationRelativeTo(null);
	  setDefaultCloseOperation(EXIT_ON_CLOSE);
  }
  
  
  private void createComboBox(){
	  comboBox = new JComboBox();
	  comboBox.addItem(" MODEL 1 ");
	  comboBox.addItem(" MODEL 2 ");
	  comboBox.addItem(" MODEL 3 ");
	  comboBox.addItem(" MODEL 4 ");
	  comboBox.addItem(" MODEL 5 ");
  }
  
  public String getSelected(){
	return (String) comboBox.getSelectedItem();  
  }

  private void createTextField(){
	  inputLabel = new JLabel("size star: ");
	  textInput = new JTextField(10);
	  textResult = new JTextArea("",15,20);
  }
 
  private void createButton(){
	  button = new JButton(" Submit ");
	  
  }
  
  public JButton getButton(){
	   return button;
   }
 
  private void createPanel(){
	  panel = new JPanel();
	  panel.add(textResult);
	  panel.add(inputLabel);
	  panel.add(textInput);
	  panel.add(comboBox);
	  panel.add(button);
	  add(panel);
	    
  }
  
  
  
  public String getSizeStar() {	  
	  return textInput.getText();
	
  }
  

  public void setResult(ArrayList<String> result){
	  String t = "";
	  for (String string : result) {
		t = t + string;
		textResult.setText(t);
	}
  }
  
} 