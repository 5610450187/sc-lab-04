package Control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ObjectInputStream.GetField;



import java.util.ArrayList;

import View.FrameStar;
import Model.Content;

public class Controller {
	private Content content;
	private FrameStar frame;
	
	public static void main(String[] args) {
		new Controller();
	}
	public Controller() {
		content =  new Content();
		frame = new FrameStar();
		
		ActionListener listener = new AddInterestListener();
		frame.getButton().addActionListener(listener);

	}
	 class AddInterestListener implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {
        	String size = frame.getSizeStar();
        	String select = frame.getSelected();
        	content.star(select,size);
    		ArrayList<String> result = content.getStar();
    		frame.setResult(result);
        	
        }
     }


}
